//
//  RatingControl.swift
//  Restoran
//
//  Created by Mac Mini on 20/08/18.
//  Copyright © 2018 Mac Mini. All rights reserved.
//

import UIKit

@IBDesignable class RatingControl: UIStackView {
    
    //MARK: Properties
        
    private var ratingButtons = [UIButton]()
    
    var rating = 0 {
        didSet {
            updateButtonSelectionStates()
        }
    }
    
    @IBInspectable var starSize: CGSize = CGSize(width: 44.0, height: 44.0){
        didSet{
            setupButtons()
        }
    }
    
    @IBInspectable var starCount: Int = 5{
        didSet{
            setupButtons()
        }
    }
        
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupButtons()
    }
    
    required init(coder: NSCoder) {
        super.init(coder: coder)
        
        setupButtons()
    }
    
    //MARK: Button Action
    
    @objc func ratingButtonTapped(button: UIButton){
        guard let index = ratingButtons.index(of: button) else {
            fatalError("The Button, \(button), is not in the rating buttons array: \(ratingButtons)")
        }
        
        let selectedRating = index + 1
        
        if selectedRating == rating {
            rating = 0
        }else{
            rating = selectedRating
        }
    }
    
    //MARK: Private Method
    
    private func setupButtons(){
        
        for button in ratingButtons{
            removeArrangedSubview(button)
            button.removeFromSuperview()
        }
        
        ratingButtons.removeAll()
        
        // Load Button Images
        let bundle = Bundle(for: type(of: self))
        let filledStar = UIImage(named: "filledStar", in: bundle, compatibleWith: self.traitCollection)
        let emptyStar = UIImage(named:"emptyStar", in: bundle, compatibleWith: self.traitCollection)
        let highlightedStar = UIImage(named:"highlightedStar", in: bundle, compatibleWith: self.traitCollection)
        
        for index in 0..<5 {
            
            // create button
            let button = UIButton()
            button.backgroundColor = UIColor.white
            
            // Set the button images
            button.setImage(emptyStar, for: .normal)
            button.setImage(filledStar, for: .selected)
            button.setImage(highlightedStar, for: .highlighted)
            button.setImage(highlightedStar, for: [.highlighted, .selected])
            
            // Add Constraints
            button.translatesAutoresizingMaskIntoConstraints = false
            button.heightAnchor.constraint(equalToConstant: 44.0).isActive = true
            button.widthAnchor.constraint(equalToConstant: 44.0).isActive = true
            
            //set the accessbillity  label
            button.accessibilityLabel = "Set \(index + 1) star rating"
            
            // Setup Button Actions
            button.addTarget(self, action: #selector(RatingControl.ratingButtonTapped(button:)), for: .touchUpInside)
            
            // Add the button to the stack
            addArrangedSubview(button)
            
            // Add the new button rating array
            ratingButtons.append(button)
        
            }
        
            updateButtonSelectionStates()
        
        }
    
    private func updateButtonSelectionStates() {
        for (index, button) in ratingButtons.enumerated(){
            button.isSelected = index < rating
            
            let hintString: String?
            if rating == index + 1{
                hintString = "Tap To Reset Rating To Zero"
            }else{
                hintString = nil
            }
            
            let valueString: String
            
            switch (rating){
            case 0:
                valueString = "No Rating Set"
            case 1:
                valueString = "1 Star Set"
            default:
                valueString = "\(rating) star set"
                
            }
            
            button.accessibilityHint = hintString
            button.accessibilityValue = valueString
            
        }
        
    }
    
}


