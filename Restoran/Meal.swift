//
//  Meal.swift
//  Restoran
//
//  Created by Mac Mini on 21/08/18.
//  Copyright © 2018 Mac Mini. All rights reserved.
//

import UIKit
import os.log

class Meal: NSObject, NSCoding {
    
    
    struct Propertykey {
        static let name = "name"
        static let photo = "photo"
        static let rating = "rating"
    }
    
    var name:String
    var photo: UIImage?
    var rating: Int
    
    static let DocumentsDirectory = FileManager().urls(for: .documentDirectory, in: .userDomainMask).first!
    static let ArchiveURL = DocumentsDirectory.appendingPathComponent("meals")
   
    func encode(with aCoder: NSCoder) {
        aCoder.encode(name, forKey: Propertykey.name)
        aCoder.encode(photo, forKey: Propertykey.photo)
        aCoder.encode(rating, forKey: Propertykey.rating)
    }
    
    required convenience init?(coder aDecoder: NSCoder){
        guard let name = aDecoder.decodeObject(forKey: Propertykey.name) as? String else{
            os_log("unable to decode object", log: OSLog.default, type: .debug)
            return nil
        }
        
        let photo = aDecoder.decodeObject(forKey: Propertykey.photo) as? UIImage
        
        let rating = aDecoder.decodeInteger(forKey: Propertykey.rating)
        
        self.init(name: name, photo: photo, rating: rating)
    }
    
    init?(name: String, photo: UIImage?, rating: Int) {
        
        // The name must not be empty
        guard !name.isEmpty else {
            return nil
        }
        
        // The rating must be between 0 and 5 inclusively
        guard (rating >= 0) && (rating <= 5) else {
            return nil
        }
        
        // Initialize stored properties.
        self.name = name
        self.photo = photo
        self.rating = rating
        
    }
    
    
    
}
